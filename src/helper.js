const emptyString = '';

export const prepare = (o) => {
  o._id = o._id.toString();
  return o;
};

export const addTypeDefs = (...args) => {
  const genSchema = (typeDefArray, property, furtherJoin) => typeDefArray
    .map((typedef) => {
      if (typedef[`${property}`]) {
        if (furtherJoin) {
          return typedef[`${property}`].join('\n');
        }
        return typedef[`${property}`];
      }
      return emptyString;
    })
    .join('\n');

  const scalarsSchema = genSchema(args, 'scalar', true);  

  const querySchema = `type Query {
    ${genSchema(args, 'query', true)}
  }`;

  const mutationSchema = `type Mutation {
    ${genSchema(args, 'mutation', true)}
  }`;

  const inputSchema = genSchema(args, 'input');
  const outputSchema = genSchema(args, 'output');

  const typeDefs = `
  ${scalarsSchema}

  ${inputSchema}
  
  ${outputSchema}

  ${querySchema}

  ${mutationSchema}

  schema {
      query: Query
      mutation: Mutation
  }`;

  return typeDefs;
};
