// babel
import 'babel-core/register';
import 'babel-polyfill';

// module imports
import { merge } from 'lodash';
import { makeExecutableSchema } from 'graphql-tools';
import express from 'express';
import bodyParser from 'body-parser';
import { graphqlExpress, graphiqlExpress } from 'graphql-server-express';
import { GQLSchemas } from 'pos-database';

// file imports
import { addTypeDefs } from './helper';
import { currentStateTypeDefs, currentStateResolvers } from './schema/currentState';
import { productTypeDefs, productResolvers } from './schema/product';
import { transactionTypeDefs, transactionResolvers } from './schema/transaction';
import { userTypeDefs, userResolvers } from './schema/user';
import { mongooseConnect } from './mongoose';

const PORT = 3001;
const URL = 'http://localhost';

// Connecting to mongodb
mongooseConnect();

// Joining all typedefs
const typeDefs = addTypeDefs(
  GQLSchemas.typeDefs,
  currentStateTypeDefs,
  productTypeDefs,
  transactionTypeDefs,
  userTypeDefs,
);

// console.log(typeDefs);

// Merging all resolvers, see merge function from lodash
const resolvers = merge(
  GQLSchemas.resolvers,
  currentStateResolvers,
  productResolvers,
  transactionResolvers,
  userResolvers,
);

// console.dir(resolvers, { depth: 2, colors: true });

const schema = makeExecutableSchema({
  typeDefs,
  resolvers,
});

const app = express();

app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));

const homePath = '/graphiql';

app.use(homePath, graphiqlExpress({
  endpointURL: '/graphql',
}));

app.listen(PORT, () => {
  console.log(`Visit ${URL}:${PORT}${homePath}`);
});
