import { models, GQLSchemas } from 'pos-database';

import { prepare } from '../helper';

const { CurrentStateGQLSchema, CurrentStateGQLInputSchema } = GQLSchemas;
const { CurrentState } = models;

export const currentStateTypeDefs = {
  output: CurrentStateGQLSchema,
  input: CurrentStateGQLInputSchema,
  query: [
    'currentState(_id: String): CurrentState',
    'currentStates: [CurrentState]'],
  mutation: ['createCurrentState(currentState: CurrentStateInput): CurrentState'],
};

export const currentStateResolvers = {
  Query: {
    currentState: async (root, { _id }) => prepare(await CurrentState.findById(_id)),
    currentStates: async () => (await CurrentState.find()).map(prepare),
  },
  Mutation: {
    createCurrentState: async (root, args) => {
      // pass it through a validator
      const currentState = CurrentState(args.currentState);
      try {
        return prepare(await currentState.save());
      } catch (err) {
        throw err;
      }
    },
  },
};
