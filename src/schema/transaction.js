import { models, GQLSchemas } from 'pos-database';

import { prepare } from '../helper';

const { TransactionGQLSchema, TransactionGQLInputSchema } = GQLSchemas;
const { Transaction } = models;

export const transactionTypeDefs = {
  output: TransactionGQLSchema,
  input: TransactionGQLInputSchema,
  query: [
    'transaction(_id: String): Transaction',
    'transactions: [Transaction]'],
  mutation: ['createTransaction(transaction: TransactionInput): Transaction'],
};

export const transactionResolvers = {
  Query: {
    transaction: async (root, { _id }) => prepare(await Transaction.findById(_id)),
    transactions: async () => (await Transaction.find()).map(prepare),
  },
  Mutation: {
    createTransaction: async (root, args) => {
      // pass it through a validator
      const transaction = Transaction(args.transaction);
      try {
        return prepare(await transaction.save());
      } catch (err) {
        throw err;
      }
    },
  },
};
