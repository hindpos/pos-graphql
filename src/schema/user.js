import { models, GQLSchemas } from 'pos-database';

import { prepare } from '../helper';

const { UserGQLSchema, UserGQLInputSchema } = GQLSchemas;
const { User } = models;

export const userTypeDefs = {
  output: UserGQLSchema,
  input: UserGQLInputSchema,
  query: [
    'user(_id: String): User',
    'users: [User]'],
  mutation: ['createUser(user: UserInput): User'],
};

export const userResolvers = {
  Query: {
    user: async (root, { _id }) => prepare(await User.findById(_id)),
    users: async () => (await User.find()).map(prepare),
  },
  Mutation: {
    createUser: async (root, args) => {
      // pass it through a validator
      const user = User(args.user);
      try {
        return prepare(await user.save());
      } catch (err) {
        throw err;
      }
    },
  },
};
