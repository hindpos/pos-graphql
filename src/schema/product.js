import { models, GQLSchemas } from 'pos-database';

import { prepare } from '../helper';

const { ProductGQLSchema, ProductGQLInputSchema } = GQLSchemas;
const { Product } = models;

export const productTypeDefs = {
  output: ProductGQLSchema,
  input: ProductGQLInputSchema,
  query: [
    'product(_id: String): Product',
    'products: [Product]'],
  mutation: ['createProduct(product: ProductInput): Product'],
};

export const productResolvers = {
  Query: {
    product: async (root, { _id }) => prepare(await Product.findById(_id)),
    products: async () => (await Product.find()).map(prepare),
  },
  Mutation: {
    createProduct: async (root, args) => {
      // pass it through a validator
      const product = Product(args.product);
      try {
        return prepare(await product.save());
      } catch (err) {
        throw err;
      }
    },
  },
};
